### Dependencies
Ionic
[Angular formly](https://github.com/formly-js/angular-formly)
[Angular input masks](https://github.com/assisrafael/angular-input-masks)
[Angular formly ionic](https://github.com/formly-js/angular-formly-templates-ionic)
[fix-mask-input](https://github.com/ivanguimam/fix-mask-input.git)
```html
<script src="lib/ionic/js/ionic.bundle.js"></script>
<script src="lib/api-check/dist/api-check.min.js"></script>
<script src="lib/angular-formly/dist/formly.min.js"></script>
<script src="lib/angular-formly-templates-ionic/dist/angular-formly-templates-ionic.js"></script>
<script src="lib/service-form/service-form.js"></script>
<script src="lib/angular-input-masks/angular-input-masks-standalone.min.js"></script>
<script src="lib/fix-mask-input/fix-mask-input.js"></script>
<script src="lib/angular-i18n/angular-locale_YOUR-LOCALE.js"></script>
```

### Insert module
```javascript
angular.module('myModule', ['findi.serviceForm'])
```

### Use
```html
<service-form ng-model="myModel" options="inputOptions"></service-form>
```
### inputOptions
```javascript
{
	configFields: { // optional
		activated: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			key: 'STRING' // Change variable from model
		},
		title: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		unitaryValue: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		description: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		discount: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		}
	},
	submit: function(data, form) { // required
		// data: myModel
		// form: form
	},
	submitButtonText: 'STRING' // Text button submit
}
```
