(function() {
	'use strict'

	angular.module('findi.serviceForm', ['ionic', 'formlyIonic', 'ui.utils.masks',
			'fixMaskInput', 'ionicUpload'
		])
		.component('serviceForm', {
			template: '<form name="{{$ctrl.opts.formName}}" novalidate="novalidate" ng-submit="$ctrl.opts.submit($ctrl.ngModel)">' +
				'<formly-form model="$ctrl.ngModel" fields="$ctrl.opts.fields"></formly-form>' +
				'<input class="findi-btn" type="submit" value="{{$ctrl.opts.submitButtonText}}"/>' +
				'<div ng-transclude></div>' +
				'</form>',
			controller: userFormController,
			controllerAs: '$ctrl',
			transclude: true,
			bindings: {
				ngModel: '=',
				options: '='
			}
		})

	function userFormController($scope, formlyConfig) {
		var vm = this
		var optionsParams = angular.copy(vm.options) || {}
		vm.opts = _buildOpts(vm, optionsParams)

		_createTextareaType(formlyConfig, optionsParams)
	}

	var _createTextareaType = function(formlyConfig, optionsParams) {
		var formlyTypes = formlyConfig.getTypes()

		if (!formlyTypes['stacked-textarea']) {
			formlyConfig.setType({
				name: 'stacked-textarea',
				template: '<label class="item item-input item-stacked-label">' +
					'<span class="input-label" aria-label="{{options.templateOptions.label}}"' +
					'id="stacked-textarea">{{options.templateOptions.label}}</span>' +
					'<textarea rows="{{options.templateOptions.row || 5}}"' +
					'placeholder="{{options.templateOptions.placeholder}}" ng-model="model[options.key]"' +
					'id="stacked-textarea" name="stacked-textarea"' +
					'formly-custom-validation="options.validators">' +
					'</textarea>' +
					'</label>'
			})
		}

		if (!formlyTypes['ion-toggle']) {
			formlyConfig.setType({
				name: 'ion-toggle',
				template: '<div class="item item-toggle">{{options.templateOptions.label}}' +
					'<label class="toggle toggle-findi">' +
					'<input type="checkbox" ng-model="model[options.key]"/>' +
					'<div class="track">' +
					'<div class="handle"></div>' +
					'</div>' +
					'</label>' +
					'</div>'
			})
		}

		if (!formlyTypes.photo) {
			var configFields = optionsParams.configFields || {}
			var configFieldPhoto = configFields.photo || {}

			formlyConfig.setType({
				name: 'photo',
				template: '<ionic-upload ng-model="model[options.key]"' +
					'options="options.templateOptions.config" formly-custom-validation="options.validators"></ionic-upload>'
			})
		}
	}

	var _buildOpts = function(vm, optionsParams) {
		var opts = {}
		opts.formName = "$ctrl.registerServiceForm"
		opts.configFields = optionsParams.configFields || {}
		opts.submitButtonText = optionsParams.submitButtonText || 'Continuar'
		opts.fields = _getFields(opts.configFields)
		opts.submit = function(data) {
			optionsParams.submit(data, vm.registerServiceForm)
		}

		return opts
	}

	var _getFields = function(configFields) {
		var array = []
		var configFieldPhoto = configFields.photo || {}
		var activatedConfigField = configFields.activated || {}
		var titleConfigField = configFields.title || {}
		var unitaryValueConfigField = configFields.unitaryValue || {}
		var descriptionConfigField = configFields.description || {}
		var discountConfigField = configFields.discount || {}

		if (!configFieldPhoto.hide) array.push(_getPhotoField(configFieldPhoto))
		if (!activatedConfigField.hide) array.push(_getActivatedField(
			activatedConfigField))
		if (!titleConfigField.hide) array.push(_getTitleField(titleConfigField))
		if (!unitaryValueConfigField.hide) array.push(_getUnitaryValueField(
			unitaryValueConfigField))
		if (!discountConfigField.hide) array.push(_getDiscountField(
			discountConfigField))
		if (!descriptionConfigField.hide) array.push(_getDescriptionField(
			descriptionConfigField))

		return array
	}

	var _getPhotoField = function(configField) {
		return {
			key: configField.key ? configField.key : "photo_url",
			type: "photo",
			templateOptions: {
				config: configField ? configField : {}
			}
		}
	}

	var _getActivatedField = function(configField) {
		return {
			key: configField.key ? configField.key : 'activated',
			type: 'ion-toggle',
			templateOptions: {
				label: configField.label ? configField.label : 'Ativo'
			}
		}
	}

	var _getTitleField = function(configField) {
		return {
			key: configField.key ? configField.key : "title",
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "text",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'TíTULO'
			}
		}
	}

	var _getUnitaryValueField = function(configField) {
		return {
			key: configField.key ? configField.key : "unit_price",
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "tel",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'VALOR UNITÁRIO',
				mask: '',
				maskInput: ''
			},
			ngModelAttrs: {
				mask: {
					attribute: 'ui-money-mask'
				},
				maskInput: {
					attribute: 'fix-mask-input'
				}
			}
		}
	}

	var _getDescriptionField = function(configField) {
		return {
			key: configField.key ? configField.key : "description",
			type: "stacked-textarea",
			templateOptions: {
				required: configField.required ? configField.required : true,
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'DESCRIÇÃO'
			}
		}
	}

	var _getDiscountField = function(configField) {
		return {
			key: configField.key ? configField.key : "discount",
			type: "stacked-input",
			templateOptions: {
				type: "tel",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'DESCONTO',
				max: 1,
				mask: '',
				maskInput: ''
			},
			ngModelAttrs: {
				mask: {
					attribute: 'ui-percentage-mask'
				},
				maskInput: {
					attribute: 'fix-mask-input'
				}
			}
		}
	}
})()
